#!/usr/bin/ruby
require "json"
require "typhoeus"

BEARER_TOKEN = ENV["BEARER_TOKEN"]

TWEET_LOOKUP_URL = "https://api.twitter.com/2/tweets"
USER_LOOKUP_URL = "https://api.twitter.com/2/users"

def tweet_unroll(tweet_id)
  params = {
    ids: tweet_id,
    # "expansions": "author_id,referenced_tweets.id",
    "tweet.fields":
      "attachments,conversation_id,entities,id,in_reply_to_user_id,lang,referenced_tweets,author_id"
    # "user.fields": "name"
    # "media.fields": "url",
    # "place.fields": "country_code",
    # "poll.fields": "options"
  }
  response = tweet_lookup(params)
  tweet = JSON.parse(response.body)["data"][0]
  text = ""
  if tweet["referenced_tweets"]
    if (
         tweet["referenced_tweets"].length >= 0 &&
           tweet["referenced_tweets"][0]["type"] == "replied_to"
       )
      meta, text = tweet_unroll(tweet["referenced_tweets"][0]["id"])
    end
  else
    params = {
      ids: tweet["author_id"],
      # "expansions": "author_id,referenced_tweets.id",
     #"tweet.fields":
     #          "attachments,conversation_id,entities,id,in_reply_to_user_id,lang,referenced_tweets"
      "user.fields": "name"
      # "media.fields": "url",
      # "place.fields": "country_code",
      # "poll.fields": "options"
    }
    author_json = JSON.parse(author_lookup(params).body)["data"][0]
    meta = {
      author: {
        name: author_json["name"],
        username: author_json["username"],
        id: author_json["id"],
      },
      thread_start: tweet["id"],
      series_title: "UNKNOWN_SERIES",
      thread_title: "UNKNOWN_THREAD",
      series_number: "UNKNOWN_NUMBER"
    }

  end
  text << tweet["text"] << "\n\n"
  return meta, text
end

def tweet_lookup(params)
  options = {
    method: "get",
    headers: {
      "User-Agent": "v2TweetLookupRuby",
      Authorization: "Bearer #{BEARER_TOKEN}"
    },
    params: params
  }

  request = Typhoeus::Request.new(TWEET_LOOKUP_URL, options)
  response = request.run
  while JSON.parse(response.body)["title"] == "Too Many Requests"
    $stderr.puts "Too many requests, waiting 15 minutes."
    sleep(15 * 60)
    response = request.run
  end
  return response
end


def author_lookup(params)
  options = {
    method: "get",
    headers: {
      "User-Agent": "v2UserLookupRuby",
      Authorization: "Bearer #{BEARER_TOKEN}"
    },
    params: params
  }

  request = Typhoeus::Request.new(USER_LOOKUP_URL, options)
  response = request.run

  while JSON.parse(response.body)["title"] == "Too Many Requests"
    $stderr.puts "Too many requests, waiting 15 minutes."
    sleep(15 * 60)
    response = request.run
  end

  return response
end

tweet_ids = []
ARGV.each { |t| 
  id = t.match('([^\/]+$)')
  tweet_ids.push(id)
}

results = []
meta = nil

tweet_ids.each { |t| 
  meta_new, tweet = tweet_unroll(t)
  if not meta
    meta = meta_new
  end
  results.push(tweet)
}

real_results = {
  metadata: meta,
  text: results
}
puts JSON.pretty_generate(real_results)
