#!/usr/bin/ruby
require "json"

all = JSON.parse(ARGF.read)

meta = all["metadata"]
series = meta["series_title"]
title = meta["thread_title"]
author = meta["author"]
series = "" if meta["series_title"] == "UNKNOWN_SERIES"
title = "" if meta["thread_title"] == "UNKNOWN_THREAD"
inter = "--" if not series.empty? and not title.empty?

puts "#{series} #{inter} #{title}"
puts "By #{author["name"]} (@#{author["id"]})"
puts "-"*80


all["text"].each { |t| puts t}
